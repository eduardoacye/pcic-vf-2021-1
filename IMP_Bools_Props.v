From Coq Require Import Arith.Arith.
From Coq Require Import Numbers.Natural.Abstract.NDiv.
From Coq Require Import Arith.Wf_nat.
From Coq Require Import Lia.
Import Nat.

From Enums Require Import IMP_Bools_Defs.
From Enums Require Import IMP_Ariths_Defs.
From Enums Require Import IMP_Ariths_Props.
From Enums Require Import Tuples_Props.
From Enums Require Import EnumType_Defs.
From Enums Require Import NatMod_Props.

Theorem enum_bool_rank' : forall b, exists pos, enum_bool b pos.
Proof.
  intros b. induction b.
  - exists 0. apply EB_T.
  - exists 1. apply EB_F.
  - destruct (enum_arith_rank' a1) as [pos1 H1].
    destruct (enum_arith_rank' a2) as [pos2 H2].
    destruct (arank_injective pos1 pos2) as [pos H].
    exists (S (S (5 * pos))).
    Check EB_Eq.
    apply (EB_Eq a1 a2 pos1 pos2 H1 H2).
    apply H.
  - destruct (enum_arith_rank' a1) as [pos1 H1].
    destruct (enum_arith_rank' a2) as [pos2 H2].
    destruct (arank_injective pos1 pos2) as [pos H].
    exists (S (S (S (5 * pos)))).
    Check EB_Lt.
    apply (EB_Lt a1 a2 pos1 pos2 H1 H2).
    apply H.
  - destruct IHb as [pos IHb].
    exists (S (S (S (S (5 * pos))))).
    apply EB_Not. apply IHb.
  - destruct IHb1 as [pos1 IHb1].
    destruct IHb2 as [pos2 IHb2].
    destruct (arank_injective pos1 pos2) as [pos H].
    exists (S (S (S (S (S (5 * pos)))))).
    apply (EB_Or b1 b2 pos1 pos2 IHb1 IHb2).
    apply H.
  - destruct IHb1 as [pos1 IHb1].
    destruct IHb2 as [pos2 IHb2].
    destruct (arank_injective pos1 pos2) as [pos H].
    exists (S (S (S (S (S (S (5 * pos))))))).
    apply (EB_And b1 b2 pos1 pos2 IHb1 IHb2).
    apply H.
Qed.

Theorem enum_bool_rank : forall b, exists! pos, enum_bool b pos.
Proof.
  intros b.
  destruct (enum_bool_rank' b) as [pos H].
  exists pos. split. apply H.
  induction H; try(intros pos' H'; inversion H'; try(reflexivity)).
  - subst.
    destruct (enum_arith_rank a1) as [pos1' [H1' H1forall]].
    destruct (enum_arith_rank a2) as [pos2' [H2' H2forall]].
    rewrite <- (H1forall pos1 H1). rewrite <- (H1forall pos0 H5).
    rewrite <- (H2forall pos2 H2). rewrite <- (H2forall pos3 H7).
    reflexivity.
  - subst.
    destruct (enum_arith_rank a1) as [pos1' [H1' H1forall]].
    destruct (enum_arith_rank a2) as [pos2' [H2' H2forall]].
    rewrite <- (H1forall pos1 H1). rewrite <- (H1forall pos0 H5).
    rewrite <- (H2forall pos2 H2). rewrite <- (H2forall pos3 H7).
    reflexivity.
  - apply IHenum_bool in H1. subst. reflexivity.
  - apply IHenum_bool1 in H4.
    apply IHenum_bool2 in H5.
    subst. reflexivity.
  - apply IHenum_bool1 in H4.
    apply IHenum_bool2 in H5.
    subst. reflexivity.
Qed.

Theorem enum_bool_unrank' : forall pos, exists b, enum_bool b pos.
Proof.
  intros pos. induction pos using lt_wf_ind.
  rename H into IH.
  (* Valores de verdad *)
  destruct pos.
  exists BT. apply EB_T.
  destruct pos.
  exists BF. apply EB_F.
  destruct (nat_mult5 pos) as [pos' [H | [H | [H | [H | H]]]]].
  - subst.
    destruct (aunrank pos') as (pos1, pos2) eqn:Upos'.
    destruct (enum_arith_unrank' pos1) as [a1 H1].
    destruct (enum_arith_unrank' pos2) as [a2 H2].
    exists (BEq a1 a2).
    apply (EB_Eq a1 a2 pos1 pos2 H1 H2).
    assert (Hinv := arank_unrank_inv pos1 pos2 pos').
    apply Hinv. apply Upos'.
  - subst.
    destruct (aunrank pos') as (pos1, pos2) eqn:Upos'.
    destruct (enum_arith_unrank' pos1) as [a1 H1].
    destruct (enum_arith_unrank' pos2) as [a2 H2].
    exists (BLt a1 a2).
    apply (EB_Lt a1 a2 pos1 pos2 H1 H2).
    assert (Hinv := arank_unrank_inv pos1 pos2 pos').
    apply Hinv. apply Upos'.
  - subst.
    assert (H := (IH pos')).
    assert (pos' < S (S (S (S (5 * pos'))))).
    { lia. }
    destruct (H H0) as [b Hb].
    exists (BNot b).
    apply EB_Not. assumption.
  - subst.
    destruct (aunrank pos') as (pos1, pos2) eqn:Upos'.
    assert (H1 := (IH pos1)).
    assert (H2 := (IH pos2)).
    destruct (aunrank_le pos' pos1 pos2 Upos').
    assert (pos1 < S (S (S (S (S (5 * pos')))))) as H1'.
    { lia. }
    assert (pos2 < S (S (S (S (S (5 * pos')))))) as H2'.
    { lia. }
    destruct (H1 H1') as [b1 H1b1].
    destruct (H2 H2') as [b2 H2b2].
    exists (BOr b1 b2).
    apply (EB_Or b1 b2 pos1 pos2 H1b1 H2b2).
    assert (Hinv := arank_unrank_inv pos1 pos2 pos').
    apply Hinv. assumption.
  - subst.
    destruct (aunrank pos') as (pos1, pos2) eqn:Upos'.
    assert (H1 := (IH pos1)).
    assert (H2 := (IH pos2)).
    destruct (aunrank_le pos' pos1 pos2 Upos').
    assert (pos1 < S (S (S (S (S (S (5 * pos'))))))) as H1'.
    { lia. }
    assert (pos2 < S (S (S (S (S (S (5 * pos'))))))) as H2'.
    { lia. }
    destruct (H1 H1') as [b1 H1b1].
    destruct (H2 H2') as [b2 H2b2].
    exists (BAnd b1 b2).
    apply (EB_And b1 b2 pos1 pos2 H1b1 H2b2).
    assert (Hinv := arank_unrank_inv pos1 pos2 pos').
    apply Hinv. assumption.
Qed.

Theorem enum_bool_unrank : forall pos, exists! b, enum_bool b pos.
Proof.
  intros pos.
  destruct (enum_bool_unrank' pos) as [b H].
  exists b. split. assumption.
  induction H;
    try (intros b' H'; inversion H';
         try(contradict H1; lia);
         try(reflexivity)).
  - rename H0 into H0'.
    assert (pos4 = pos) as H0. lia.
    clear H0'.
    rewrite H0 in *.
    destruct (arank_argeq pos0 pos3 pos1 pos2 pos H6 H3).
    subst.
    destruct (enum_arith_unrank pos1) as [a1' [_ Harith1]].
    rewrite <- (Harith1 a1 H1).
    rewrite <- (Harith1 a0 H4).
    destruct (enum_arith_unrank pos2) as [a2' [_ Harith2]].
    rewrite <- (Harith2 a2 H2).
    rewrite <- (Harith2 a3 H5).
    reflexivity.
  - rename H0 into H0'.
    assert (pos4 = pos) as H0. lia.
    clear H0'.
    rewrite H0 in *.
    destruct (arank_argeq pos0 pos3 pos1 pos2 pos H6 H3).
    subst.
    destruct (enum_arith_unrank pos1) as [a1' [_ Harith1]].
    rewrite <- (Harith1 a1 H1).
    rewrite <- (Harith1 a0 H4).
    destruct (enum_arith_unrank pos2) as [a2' [_ Harith2]].
    rewrite <- (Harith2 a2 H2).
    rewrite <- (Harith2 a3 H5).
    reflexivity.
  - rename H0 into H0'.
    assert (pos0 = pos) as H0.
    { lia. }
    clear H0'.
    rewrite H0 in *.
    apply f_equal.
    apply (IHenum_bool b0 H2).
  - rename H1 into H1'.
    assert (pos4 = pos) as H1.
    { lia. }
    clear H1'.
    rewrite H1 in *.
    destruct (arank_argeq pos0 pos3 pos1 pos2 pos H6 H3).
    subst.
    rewrite (IHenum_bool1 b0 H2).
    rewrite (IHenum_bool2 b3 H4).
    reflexivity.
  - rename H1 into H1'.
    assert (pos4 = pos) as H1.
    { lia. }
    clear H1'.
    rewrite H1 in *.
    destruct (arank_argeq pos0 pos3 pos1 pos2 pos H6 H3).
    subst.
    rewrite (IHenum_bool1 b0 H2).
    rewrite (IHenum_bool2 b3 H4).
    reflexivity.
Qed.

Goal (enumerable_type enum_bool).
Proof.
  split.
  - apply enum_bool_rank.
  - apply enum_bool_unrank.
Qed.
