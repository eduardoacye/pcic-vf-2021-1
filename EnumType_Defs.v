Definition enum_rel (T : Type) := T -> nat -> Prop.

Definition enumerable_type {T : Type} (R : enum_rel T) :=
  (forall x, exists! pos, R x pos) /\
  (forall pos, exists! x, R x pos).
