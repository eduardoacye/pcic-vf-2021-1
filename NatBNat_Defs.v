Inductive bnat : Type :=
| Z
| U (bn : bnat)
| D (bn : bnat).

Fixpoint bnat_succ (bn : bnat) : bnat :=
  match bn with
  | Z => U Z
  | U x => D x
  | D x => U (bnat_succ x)
  end.

Fixpoint nat_to_bnat (n : nat) : bnat :=
  match n with
  | O => Z
  | S n' => bnat_succ (nat_to_bnat n')
  end.

Fixpoint bnat_to_nat (bn : bnat) : nat :=
  match bn with
  | Z => O
  | U x => S (2 * (bnat_to_nat x))
  | D x => S (S (2 * (bnat_to_nat x)))
  end.
