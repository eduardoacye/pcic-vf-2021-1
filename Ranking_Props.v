From Coq Require Import Lia.

From Enums Require Import Ranking_Defs.
From Enums Require Import MixedRanking_Defs.
From Enums Require Import MixedRanking_Props.
From Enums Require Import NatBNat_Defs.
From Enums Require Import NatBNat_Props.
From Enums Require Import Tuples_Defs.

Theorem rank_tuple_injective : (rank_injective rank_tuple).
Proof.
  unfold rank_injective.
  intros.
  destruct (mixed_rank_injective a b) as [n H].
  unfold rank_tuple. rewrite H.
  exists (bnat_to_nat n).
  reflexivity.
Qed.

Theorem rank_tuple_surjective : (rank_surjective rank_tuple).
Proof.
  unfold rank_surjective.
  intros.
  destruct (mixed_rank_surjective (nat_to_bnat n)) as [a [b H]].
  unfold rank_tuple. exists a. exists b. rewrite H.
  rewrite bnat_nat_equiv.
  reflexivity.
Qed.

Theorem unrank_tuple_injective : (unrank_injective unrank_tuple).
Proof.
  unfold unrank_injective.
  intros.
  destruct (mixed_unrank_injective (nat_to_bnat n)) as [a [b H]].
  unfold unrank_tuple. exists a. exists b. rewrite H.
  reflexivity.
Qed.

Theorem unrank_tuple_surjective : (unrank_surjective unrank_tuple).
Proof.
  unfold unrank_surjective.
  intros.
  destruct (mixed_unrank_surjective a b) as [n H].
  unfold unrank_tuple. exists (bnat_to_nat n).
  rewrite nat_bnat_equiv.
  assumption.
Qed.

Theorem rank_unrank_tuple_inv : (rank_unrank_inv rank_tuple unrank_tuple).
Proof.
  unfold rank_unrank_inv.
  intros. unfold rank_tuple. unfold unrank_tuple.
  destruct (mixed_rank_unrank_inv a b (nat_to_bnat n)) as [H1 H2].
  split.
  - intros. apply bnat_nat_eq_inv in H. apply H1. assumption.
  - intros. apply nat_bnat_eq_inv. apply H2. assumption.
Qed.

Theorem rank_tuple_le : (rank_le rank_tuple).
Proof.
  unfold rank_le.
  unfold rank_tuple.
  intros.
  apply bnat_nat_eq_inv in H.
  apply mixed_rank_le in H.
  rewrite bnat_nat_equiv in *.
  assumption.
Qed.

Theorem unrank_tuple_le : (unrank_le unrank_tuple).
  unfold unrank_le.
  unfold unrank_tuple.
  intros.
  apply mixed_rank_unrank_inv in H.
  apply (mixed_rank_le a b (nat_to_bnat n)) in H.
  rewrite bnat_nat_equiv in *.
  assumption.
Qed.
