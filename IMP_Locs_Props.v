From Enums Require Import IMP_Locs_Defs.
From Enums Require Import IMP_Nats_Defs.
From Enums Require Import IMP_Nats_Props.
From Enums Require Import EnumType_Defs.

Theorem enum_loc_rank : forall x, exists! pos, enum_loc x pos.
Proof.
  intros [n]. exists n. split.
  - apply EL_Bypass. apply EN_Trivial.
  - intros pos. intros H. inversion H. inversion H1. reflexivity.
Qed.

Theorem enum_loc_unrank : forall pos, exists! x, enum_loc x pos.
Proof.
  intros pos. exists (X pos). split.
  - apply EL_Bypass. apply EN_Trivial.
  - intros [n]. intros H. inversion H. inversion H1. reflexivity.
Qed.

Goal (enumerable_type enum_loc).
Proof.
  split.
  - apply enum_loc_rank.
  - apply enum_loc_unrank.
Qed.
