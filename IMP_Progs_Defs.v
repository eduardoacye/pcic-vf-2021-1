From Enums Require Import IMP_Locs_Defs.
From Enums Require Import IMP_Ariths_Defs.
From Enums Require Import IMP_Bools_Defs.
From Enums Require Import Tuples_Props.
From Enums Require Import EnumType_Defs.

Inductive Prog : Type :=
| PSkip
| PSet (x : Loc) (a : Arith)
| PConc (p1 p2 : Prog)
| PWhile (b : Bool) (p : Prog)
| PIf (b : Bool) (p1 p2 : Prog).

Inductive enum_prog : (enum_rel Prog) :=
| EP_Skip : enum_prog PSkip 0
| EP_Set (x : Loc) (a : Arith)
         (pos1 pos2 : nat)
         (H1 : enum_loc x pos1)
         (H2 : enum_arith a pos2)
         (pos : nat)
         (H3 : arank pos1 pos2 = pos) :
    enum_prog (PSet x a) (S (4 * pos))
| EP_Conc (p1 p2 : Prog)
          (pos1 pos2 : nat)
          (H1 : enum_prog p1 pos1)
          (H2 : enum_prog p2 pos2)
          (pos : nat)
          (H3 : arank pos1 pos2 = pos) :
    enum_prog (PConc p1 p2) (S (S (4 * pos)))
| EP_While (b : Bool) (p : Prog)
           (pos1 pos2 : nat)
           (H1 : enum_bool b pos1)
           (H2 : enum_prog p pos2)
           (pos : nat)
           (H3 : arank pos1 pos2 = pos) :
    enum_prog (PWhile b p) (S (S (S (4 * pos))))
| EP_If (b : Bool) (p1 p2 : Prog)
        (pos1 pos2 pos3 : nat)
        (H1 : enum_bool b pos1)
        (H2 : enum_prog p1 pos2)
        (H3 : enum_prog p2 pos3)
        (pos pos' : nat)
        (H4 : arank pos2 pos3 = pos')
        (H5 : arank pos1 pos' = pos) :
    enum_prog (PIf b p1 p2) (S (S (S (S (4 * pos))))).
