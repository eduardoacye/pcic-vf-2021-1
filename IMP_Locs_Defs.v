From Enums Require Import IMP_Nats_Defs.

Inductive Loc : Type :=
| X (n : nat).

Inductive enum_loc : Loc -> nat -> Prop :=
| EL_Bypass (n pos : nat) (H : enum_nat n pos) :
    enum_loc (X n) pos.
