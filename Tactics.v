Ltac unfoldofl def := unfold def; fold def.

Ltac unfoldofl_ctx def H := unfold def in H; fold def in H.
