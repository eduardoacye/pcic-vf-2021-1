From Enums Require Import NatBNat_Defs.

Fixpoint mixed_rank (a b : nat) : bnat :=
  match a with
  | O => match b with
         | O => Z
         | S b' => D (nat_to_bnat b')
         end
  | S a' => U (mixed_rank a' b)
  end.

Fixpoint mixed_unrank (bn : bnat) : nat * nat :=
  match bn with
  | Z => (O, O)
  | U x => match (mixed_unrank x) with
           | (a, b) => (S a, b)
           end
  | D x => (O, S (bnat_to_nat x))
  end.
