From Enums Require Import IMP_Nats_Defs.
From Enums Require Import EnumType_Defs.

Theorem enum_nat_rank : forall n, exists! pos, enum_nat n pos.
Proof.
  intros n. exists n. split.
  - apply EN_Trivial.
  - intros pos. intros H. inversion H. reflexivity.
Qed.

Theorem enum_nat_unrank : forall pos, exists! n, enum_nat n pos.
Proof.
  intros pos. exists pos. split.
  - apply EN_Trivial.
  - intros n. intros H. inversion H. reflexivity.
Qed.

Goal (enumerable_type enum_nat).
Proof.
  split.
  - apply enum_nat_rank.
  - apply enum_nat_unrank.
Qed.
