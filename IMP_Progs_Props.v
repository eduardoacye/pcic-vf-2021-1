From Coq Require Import Arith.Arith.
From Coq Require Import Arith.Wf_nat.
From Coq Require Import Lia.
Import Nat.

From Enums Require Import IMP_Progs_Defs.
From Enums Require Import IMP_Locs_Defs.
From Enums Require Import IMP_Locs_Props.
From Enums Require Import IMP_Ariths_Defs.
From Enums Require Import IMP_Ariths_Props.
From Enums Require Import IMP_Bools_Defs.
From Enums Require Import IMP_Bools_Props.
From Enums Require Import Tuples_Props.
From Enums Require Import EnumType_Defs.
From Enums Require Import NatMod_Props.

Theorem enum_prog_rank' : forall p, exists pos, enum_prog p pos.
Proof.
  intros p. induction p.
  - exists 0. apply EP_Skip.
  - destruct (enum_loc_rank x) as [pos1 [H1 _]].
    destruct (enum_arith_rank' a) as [pos2 H2].
    destruct (arank_injective pos1 pos2) as [pos H].
    exists (S (4 * pos)).
    apply (EP_Set x a pos1 pos2 H1 H2).
    apply H.
  - destruct IHp1 as [pos1 IHp1].
    destruct IHp2 as [pos2 IHp2].
    destruct (arank_injective pos1 pos2) as [pos H].
    exists (S (S (4 * pos))).
    apply (EP_Conc p1 p2 pos1 pos2 IHp1 IHp2).
    apply H.
  - destruct (enum_bool_rank' b) as [pos1 H1].
    destruct IHp as [pos2 IHp].
    destruct (arank_injective pos1 pos2) as [pos H].
    exists (S (S (S (4 * pos)))).
    apply (EP_While b p pos1 pos2 H1 IHp).
    apply H.
  - destruct (enum_bool_rank' b) as [pos1 H1].
    destruct IHp1 as [pos2 IHp1].
    destruct IHp2 as [pos3 IHp2].
    destruct (arank_injective pos2 pos3) as [pos' H'].
    destruct (arank_injective pos1 pos') as [pos H].
    exists (S (S (S (S (4 * pos))))).
    apply (EP_If b p1 p2 pos1 pos2 pos3 H1 IHp1 IHp2 pos pos' H' H).
Qed.

Theorem enum_prog_rank : forall p, exists! pos, enum_prog p pos.
Proof.
  intros p.
  destruct (enum_prog_rank' p) as [pos H].
  exists pos. split. apply H.
  induction H; try(intros pos' H'; inversion H'; try(reflexivity)).
  - subst.
    destruct (enum_loc_rank x) as [pos1' [H1' H1forall]].
    destruct (enum_arith_rank a) as [pos2' [H2' H2forall]].
    rewrite <- (H1forall pos1 H1). rewrite <- (H1forall pos0 H5).
    rewrite <- (H2forall pos2 H2). rewrite <- (H2forall pos3 H7).
    reflexivity.
  - subst.
    apply IHenum_prog1 in H4.
    apply IHenum_prog2 in H5.
    subst. reflexivity.
  - subst.
    destruct (enum_bool_rank b) as [pos1' [H1' H1forall]].
    rewrite <- (H1forall pos1 H1). rewrite <- (H1forall pos0 H4).
    apply IHenum_prog in H5.
    subst. reflexivity.
  - intros pos'' H''. inversion H''.
    subst.
    destruct (enum_bool_rank b) as [pos1' [H1' H1forall]].
    rewrite <- (H1forall pos1 H1). rewrite <- (H1forall pos0 H7).
    apply IHenum_prog1 in H8.
    apply IHenum_prog2 in H9.
    subst. reflexivity.
Qed.

Theorem enum_prog_unrank' : forall pos, exists p, enum_prog p pos.
Proof.
  intros pos. induction pos using lt_wf_ind.
  rename H into IH.
  (* Skip *)
  destruct pos.
  exists PSkip. apply EP_Skip.
  (* Programas compuestos *)
  destruct (nat_mult4 pos) as [pos' [H | [H | [H | H]]]].
  - subst.
    destruct (aunrank pos') as (pos1, pos2) eqn:Upos'.
    destruct (enum_loc_unrank pos1) as [x [Hx _]].
    destruct (enum_arith_unrank' pos2) as [a Ha].
    exists (PSet x a).
    apply (EP_Set x a pos1 pos2 Hx Ha).
    apply (arank_unrank_inv pos1 pos2 pos').
    apply Upos'.
  - subst.
    destruct (aunrank pos') as (pos1, pos2) eqn:Upos'.
    assert (H1 := (IH pos1)).
    assert (H2 := (IH pos2)).
    destruct (aunrank_le pos' pos1 pos2 Upos').
    assert (pos1 < S (S (4 * pos'))) as H1'. lia.
    assert (pos2 < S (S (4 * pos'))) as H2'. lia.
    destruct (H1 H1') as [p1 H1p1].
    destruct (H2 H2') as [p2 H2p2].
    exists (PConc p1 p2).
    apply (EP_Conc p1 p2 pos1 pos2 H1p1 H2p2).
    apply (arank_unrank_inv pos1 pos2 pos').
    apply Upos'.
  - subst.
    destruct (aunrank pos') as (pos1, pos2) eqn:Upos'.
    destruct (enum_bool_unrank' pos1) as [b Hb].
    assert (H2 := (IH pos2)).
    destruct (aunrank_le pos' pos1 pos2 Upos').
    assert (pos2 < S (S (S (4 * pos')))) as H2'. lia.
    destruct (H2 H2') as [p Hp].
    exists (PWhile b p).
    apply (EP_While b p pos1 pos2 Hb Hp).
    apply (arank_unrank_inv pos1 pos2 pos').
    apply Upos'.
  - subst.
    destruct (aunrank pos') as (pos1, pos'') eqn:Upos'.
    destruct (aunrank pos'') as (pos2, pos3) eqn:Upos''.
    destruct (enum_bool_unrank' pos1) as [b Hb].
    assert (Hp1 := (IH pos2)).
    assert (Hp2 := (IH pos3)).
    destruct (aunrank_le pos' pos1 pos'' Upos').
    destruct (aunrank_le pos'' pos2 pos3 Upos'').
    assert (pos2 < S (S (S (S (4 * pos'))))) as Hp1'. lia.
    assert (pos3 < S (S (S (S (4 * pos'))))) as Hp2'. lia.
    destruct (Hp1 Hp1') as [p1 H1p1].
    destruct (Hp2 Hp2') as [p2 H2p2].
    exists (PIf b p1 p2).
    apply (EP_If b p1 p2 pos1 pos2 pos3 Hb H1p1 H2p2 pos' pos'').
    apply (arank_unrank_inv pos2 pos3 pos''). apply Upos''.
    apply (arank_unrank_inv pos1 pos'' pos'). apply Upos'.
Qed.

Theorem enum_prog_unrank : forall pos, exists! p, enum_prog p pos.
Proof.
  intros pos.
  destruct (enum_prog_unrank' pos) as [p H].
  exists p. split. assumption.
  induction H;
    try (intros b' H'; inversion H';
         try(contradict H1; lia);
         try(reflexivity)).
  - rename H0 into H0'.
    assert (pos4 = pos) as H0. lia.
    clear H0'.
    rewrite H0 in *.
    destruct (arank_argeq pos0 pos3 pos1 pos2 pos H6 H3).
    subst.
    destruct (enum_loc_unrank pos1) as [x' [_ Hloc1]].
    rewrite <- (Hloc1 x H1). rewrite <- (Hloc1 x0 H4).
    destruct (enum_arith_unrank pos2) as [a' [_ Harith2]].
    rewrite <- (Harith2 a H2). rewrite <- (Harith2 a0 H5).
    reflexivity.
  - rename H1 into H1'.
    assert (pos4 = pos) as H1. lia.
    clear H1'.
    rewrite H1 in *.
    destruct (arank_argeq pos0 pos3 pos1 pos2 pos H6 H3).
    subst.
    rewrite (IHenum_prog1 p0 H2).
    rewrite (IHenum_prog2 p3 H4).
    reflexivity.
  - rename H0 into H0'.
    assert (pos4 = pos) as H0. lia.
    clear H0'.
    rewrite H0 in *.
    destruct (arank_argeq pos0 pos3 pos1 pos2 pos H6 H3).
    subst.
    destruct (enum_bool_unrank pos1) as [b' [_ Hbool1]].
    rewrite <- (Hbool1 b H1). rewrite <- (Hbool1 b0 H2).
    rewrite (IHenum_prog p0 H4).
    reflexivity.
  - rename H2 into H2'.
    assert (pos6 = pos) as H2. lia.
    clear H2'.
    rewrite H2 in *.
    destruct (arank_argeq pos0 pos'0 pos1 pos' pos H10 H5).
    rewrite H12 in *.
    destruct (arank_argeq pos4 pos5 pos2 pos3 pos' H8 H4).
    subst.
    destruct (enum_bool_unrank pos1) as [b' [_ Hbool1]].
    rewrite <- (Hbool1 b H1). rewrite <- (Hbool1 b0 H3).
    rewrite (IHenum_prog1 p0 H6). rewrite (IHenum_prog2 p3 H7).
    reflexivity.
Qed.

Goal (enumerable_type enum_prog).
Proof.
  split.
  - apply enum_prog_rank.
  - apply enum_prog_unrank.
Qed.
