From Enums Require Import EnumType_Defs.

Inductive enum_nat : (enum_rel nat) :=
| EN_Trivial (n : nat) : enum_nat n n.
