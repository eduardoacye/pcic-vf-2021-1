From Coq Require Import Arith.Arith.
From Coq Require Import Lia.

From Enums Require Import MixedRanking_Defs.
From Enums Require Import NatBNat_Defs.
From Enums Require Import NatBNat_Props.
From Enums Require Import Tactics.

Theorem mixed_rank_injective : forall a b, exists n, mixed_rank a b = n.
Proof.
  intros a. induction a as [| a' IHa].
  - intros b. destruct b as [| b'] eqn:Eb.
    + exists Z. reflexivity.
    + exists (D (nat_to_bnat b')). reflexivity.
  - intros b. destruct b as [| b'] eqn:Eb.
    + unfoldofl mixed_rank.
      assert (IHa := IHa 0). destruct IHa as [n' IHa]. rewrite IHa.
      exists (U n'). reflexivity.
    + unfoldofl mixed_rank.
      assert (IHa := IHa (S b')). destruct IHa as [n' IHa]. rewrite IHa.
      exists (U n'). reflexivity.
Qed.

Theorem mixed_rank_surjective : forall n, exists a b, mixed_rank a b = n.
Proof.
  intros n. induction n as [| n' IHU | n' IHD].
  - exists O. exists O. simpl. reflexivity.
  - destruct IHU as [a' [b' IHU]].
    exists (S a'). exists b'.
    simpl. apply f_equal. assumption.
  - unfoldofl mixed_rank.
    destruct IHD as [a' [b' IHD]].
    induction a' as [| a'' IHa'].
    + destruct b'.
      * simpl in IHD. exists O. exists 1. simpl. subst. reflexivity.
      * exists O. exists (2 * (S b') + 1).
        simpl in IHD. rewrite <- nat_to_bnat_double in IHD.
        rewrite <- IHD.
        simpl. apply f_equal.
        rewrite <- plus_Snm_nSm. rewrite plus_Sn_m. rewrite <- plus_n_O. rewrite <- plus_n_O.
        simpl.  reflexivity.
    + simpl in IHD.
      exists O. exists (S (bnat_to_nat n')).
      simpl. rewrite nat_bnat_equiv. reflexivity.
Qed.

Theorem mixed_unrank_injective : forall n, exists a b, mixed_unrank n = (a, b).
Proof.
  intros n.
  induction n as [| n' IHU | n' IHD]; unfoldofl mixed_unrank.
  - exists O. exists O. reflexivity.
  - destruct IHU as [a' [b' IHU]].
    rewrite IHU. exists (S a'). exists b'. reflexivity.
  - exists O. exists (S (bnat_to_nat n')). reflexivity.
Qed.

Theorem mixed_unrank_surjective : forall a b, exists n, mixed_unrank n = (a, b).
Proof.
  intros a. induction a as [| a' IHa].
  - intros b. destruct b.
    + exists Z. reflexivity.
    + exists (D (nat_to_bnat b)).
      simpl. rewrite bnat_nat_equiv. reflexivity.
  - intros b.
    assert (IHa := IHa b). destruct IHa as [n' IHa].
    exists (U n'). simpl. rewrite IHa. reflexivity.
Qed.

Theorem mixed_rank_unrank_inv : forall a b n,
    mixed_rank a b = n <-> mixed_unrank n = (a, b).
Proof.
  intros a. induction a as [| a' IHa].
  - intros b. destruct b as [| b'].
    + intros. split.
      * intros. simpl in H. rewrite <- H. simpl. reflexivity.
      * intros. simpl. destruct n.
        -- reflexivity.
        -- unfoldofl_ctx mixed_unrank H.
           destruct (mixed_unrank n). discriminate H.
        -- unfoldofl_ctx mixed_unrank H. discriminate H.
    +  intros. split.
       * intros. unfoldofl_ctx mixed_rank H. rewrite <- H.
         simpl. apply f_equal. apply f_equal.
         rewrite bnat_nat_equiv. reflexivity.
       * intros. destruct n.
         -- simpl in H. discriminate H.
         -- simpl in H. destruct (mixed_unrank n). discriminate H.
         -- simpl. apply f_equal.
            simpl in H. injection H. intros. rewrite <- H0.
            rewrite nat_bnat_equiv. reflexivity.
  - intros b n.
    destruct n.
    + simpl. split; (intros; discriminate H).
    + split.
      * intros. simpl in H. injection H. intros.
        destruct (IHa b n) as [H1 H2].
        apply H1 in H0.
        simpl. rewrite H0.
        reflexivity.
      * intros. simpl. apply f_equal.
        destruct (IHa b n) as [H1 H2].
        simpl in H. destruct (mixed_unrank n).
        apply H2. injection H. intros. subst.
        reflexivity.
    + split; (intros; simpl in H; discriminate H).
Qed.

Theorem mixed_rank_le : forall a b n,
    mixed_rank a b = n ->
    a <= (bnat_to_nat n) /\ b <= (bnat_to_nat n).
Proof.
  intros a. induction a.
  - intros b. induction b.
    + intros. simpl in *. subst. lia.
    + intros n. simpl. rewrite <- nat_to_bnat_double.
      intros H. subst. simpl. rewrite <- bnat_to_nat_succ.
      split; try(lia).
      assert (b + S (b + 0) = S (2 * b)).
      { lia. }
      rewrite H. clear H.
      simpl.
      rewrite <- bnat_to_nat_succ.
      rewrite bnat_nat_equiv.
      lia.
  - intros b.
    intros. simpl in H. destruct n; try(discriminate H).
    injection H. intros. apply IHa in H0.
    destruct H0. simpl. lia.
Qed.
