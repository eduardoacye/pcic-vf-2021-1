Definition rank_func := nat -> nat -> nat.

Definition unrank_func := nat -> (nat * nat).

Definition rank_injective (f : rank_func) :=
  forall a b, exists n, f a b = n.

Definition rank_surjective (f : rank_func) :=
  forall n, exists a b, f a b = n.

Definition rank_bijective (f : rank_func) :=
  (rank_injective f) /\ (rank_surjective f).

Definition unrank_injective (g : unrank_func) :=
  forall n, exists a b, g n = (a, b).

Definition unrank_surjective (g : unrank_func) :=
  forall a b, exists n, g n = (a, b).

Definition unrank_bijective (g : unrank_func) :=
  (unrank_injective g) /\ (unrank_surjective g).

Definition rank_unrank_inv (f : rank_func) (g : unrank_func) :=
  forall a b n, f a b = n <-> g n = (a, b).

Definition rank_le (f : rank_func) :=
  forall a b n, f a b = n -> a <= n /\ b <= n.

Definition unrank_le (g : unrank_func) :=
  forall n a b, g n = (a, b) -> a <= n /\ b <= n.
