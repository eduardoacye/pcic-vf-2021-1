From Coq Require Import Arith.Arith.
From Coq Require Import Arith.Wf_nat.
From Coq Require Import Lia.
Import Nat.

From Enums Require Import IMP_Ariths_Defs.
From Enums Require Import IMP_Nats_Defs.
From Enums Require Import IMP_Nats_Props.
From Enums Require Import IMP_Locs_Defs.
From Enums Require Import IMP_Locs_Props.
From Enums Require Import Tuples_Props.
From Enums Require Import EnumType_Defs.
From Enums Require Import NatMod_Props.

Theorem enum_arith_rank' : forall a, exists pos, enum_arith a pos.
Proof.
  intros a.
  induction a.
  - exists (5 * n). apply EA_N. apply EN_Trivial.
  - destruct x. exists (S (5 * n)). apply EA_X. apply EL_Bypass. apply EN_Trivial.
  - destruct IHa1 as [pos1 IHa1].
    destruct IHa2 as [pos2 IHa2].
    destruct (arank_injective pos1 pos2) as [pos H].
    exists (S (S (5 * pos))).
    eapply EA_Plus.
    + apply IHa1.
    + apply IHa2.
    + apply H.
  - destruct IHa1 as [pos1 IHa1].
    destruct IHa2 as [pos2 IHa2].
    destruct (arank_injective pos1 pos2) as [pos H].
    exists (S (S (S (5 * pos)))).
    eapply EA_Minus.
    + apply IHa1.
    + apply IHa2.
    + apply H.
  - destruct IHa1 as [pos1 IHa1].
    destruct IHa2 as [pos2 IHa2].
    destruct (arank_injective pos1 pos2) as [pos H].
    exists (S (S (S (S (5 * pos))))).
    eapply EA_Mult.
    + apply IHa1.
    + apply IHa2.
    + apply H.
Qed.

Theorem enum_arith_rank : forall a, exists! pos, enum_arith a pos.
Proof.
  intros a.
  destruct (enum_arith_rank' a) as [pos H].
  exists pos. split. apply H.
  induction H.
  - intros pos' H'. inversion H'.
    inversion H. inversion H1.
    subst. reflexivity.
  - intros pos' H'. inversion H'.
    inversion H. inversion H3.
    subst.
    inversion H1. inversion H2.
    reflexivity.
  - intros pos' H'. inversion H'.
    apply IHenum_arith1 in H4.
    apply IHenum_arith2 in H5.
    subst. reflexivity.
  - intros pos' H'. inversion H'.
    apply IHenum_arith1 in H4.
    apply IHenum_arith2 in H5.
    subst. reflexivity.
  - intros pos' H'. inversion H'.
    apply IHenum_arith1 in H4.
    apply IHenum_arith2 in H5.
    subst. reflexivity.
Qed.

Theorem enum_arith_unrank' : forall pos, exists a, enum_arith a pos.
Proof.
  intros pos. induction pos using lt_wf_ind.
  rename H into IH.
  destruct (nat_mult5 pos) as [pos' [H | [H | [H | [H | H]]]]].
  - subst. exists (AN pos'). apply EA_N. apply EN_Trivial.
  - subst. exists (AX (X pos')). apply EA_X. apply EL_Bypass. apply EN_Trivial.
  - subst.
    destruct (aunrank pos') as (pos1, pos2) eqn:Upos'.
    assert (H1 := (IH pos1)).
    assert (H2 := (IH pos2)).
    destruct (aunrank_le pos' pos1 pos2 Upos').
    assert (pos1 < S (S (5 * pos'))) as H1'.
    { lia. }
    assert (pos2 < S (S (5 * pos'))) as H2'.
    { lia. }
    destruct (H1 H1') as [a1 H1''].
    destruct (H2 H2') as [a2 H2''].
    exists (APlus a1 a2).
    apply (EA_Plus a1 a2 pos1 pos2 H1'' H2'').
    apply (arank_unrank_inv pos1 pos2 pos').
    assumption.
  - subst.
    destruct (aunrank pos') as (pos1, pos2) eqn:Upos'.
    assert (H1 := (IH pos1)).
    assert (H2 := (IH pos2)).
    destruct (aunrank_le pos' pos1 pos2 Upos').
    assert (pos1 < S (S (S (5 * pos')))) as H1'.
    { lia. }
    assert (pos2 < S (S (S (5 * pos')))) as H2'.
    { lia. }
    destruct (H1 H1') as [a1 H1''].
    destruct (H2 H2') as [a2 H2''].
    exists (AMinus a1 a2).
    apply (EA_Minus a1 a2 pos1 pos2 H1'' H2'').
    apply (arank_unrank_inv pos1 pos2 pos').
    assumption.
  - subst.
    destruct (aunrank pos') as (pos1, pos2) eqn:Upos'.
    assert (H1 := (IH pos1)).
    assert (H2 := (IH pos2)).
    destruct (aunrank_le pos' pos1 pos2 Upos').
    assert (pos1 < S (S (S (S (5 * pos'))))) as H1'.
    { lia. }
    assert (pos2 < S (S (S (S (5 * pos'))))) as H2'.
    { lia. }
    destruct (H1 H1') as [a1 H1''].
    destruct (H2 H2') as [a2 H2''].
    exists (AMult a1 a2).
    apply (EA_Mult a1 a2 pos1 pos2 H1'' H2'').
    apply (arank_unrank_inv pos1 pos2 pos').
    assumption.
Qed.

Theorem enum_arith_unrank : forall pos, exists! a, enum_arith a pos.
Proof.
  intros pos.
  destruct (enum_arith_unrank' pos) as [a H].
  exists a. split. assumption.
  induction H.
  - intros a' H'. inversion H'; try(contradict H1; lia).
    rename H0 into H0'.
    assert (pos0 = pos) as H0. lia.
    clear H0'. rewrite H0 in *.
    inversion H. inversion H2. reflexivity.
  - intros a' H'. inversion H'; try(contradict H1; lia).
    rename H0 into H0'.
    assert (pos0 = pos) as H0. lia.
    clear H0'. rewrite H0 in *.
    inversion H. inversion H2.
    inversion H3. inversion H6.
    reflexivity.
  - intros a' H'. inversion H'; try(contradict H1; lia).
    rename H1 into H1'.
    assert (pos4 = pos) as H1. lia.
    clear H1'.
    rewrite H1 in *.
    destruct (arank_argeq pos0 pos3 pos1 pos2 pos H6 H3).
    subst.
    assert (IHenum_arith1 := IHenum_arith1 a0 H2).
    assert (IHenum_arith2 := IHenum_arith2 a3 H4).
    subst. reflexivity.
  - intros a' H'. inversion H'; try(contradict H1; lia).
    rename H1 into H1'.
    assert (pos4 = pos) as H1. lia.
    clear H1'.
    rewrite H1 in *.
    destruct (arank_argeq pos0 pos3 pos1 pos2 pos H6 H3).
    subst.
    assert (IHenum_arith1 := IHenum_arith1 a0 H2).
    assert (IHenum_arith2 := IHenum_arith2 a3 H4).
    subst. reflexivity.
  - intros a' H'. inversion H'; try(contradict H1; lia).
    rename H1 into H1'.
    assert (pos4 = pos) as H1. lia.
    clear H1'.
    rewrite H1 in *.
    destruct (arank_argeq pos0 pos3 pos1 pos2 pos H6 H3).
    subst.
    assert (IHenum_arith1 := IHenum_arith1 a0 H2).
    assert (IHenum_arith2 := IHenum_arith2 a3 H4).
    subst. reflexivity.
Qed.

Goal (enumerable_type enum_arith).
Proof.
  split.
  - apply enum_arith_rank.
  - apply enum_arith_unrank.
Qed.
