From Enums Require Import Tuples_Defs.
From Enums Require Import Ranking_Defs.
From Enums Require Import Ranking_Props.

Theorem tuple_enumeration : exists f g,
    rank_bijective f /\
    unrank_bijective g /\
    rank_unrank_inv f g /\
    rank_le f /\
    unrank_le g.
Proof.
  exists rank_tuple.
  exists unrank_tuple.
  split.
  (* Rank es biyectiva *)
  unfold rank_bijective. split.
  apply rank_tuple_injective.
  apply rank_tuple_surjective.
  split.
  (* Unrank es biyectiva *)
  unfold unrank_bijective. split.
  apply unrank_tuple_injective.
  apply unrank_tuple_surjective.
  split.
  (* Rank y unrank son inversas una de la otra *)
  apply rank_unrank_tuple_inv.
  split.
  (* Rank asocia tuplas con componentes menores o iguales a su posición *)
  apply rank_tuple_le.
  (* Unrank asocia posiciones mayores o iguales a las componentes de la tupla correspondiente *)
  apply unrank_tuple_le.
Qed.

(* Ya que existen funciones rank y unrank que satisfacen las propiedades necesarias por el
   algoritmo de enumeración, se utilizan arank y aunrank como cualesquiera dos funciones
   de estas que existen. La finalidad de esto no utilizar específicamente rank_tuple y
   unrank_tuple de Ranking_Defs.v *)

Parameter (arank : rank_func)
          (aunrank : unrank_func).

Axiom arank_injective : (rank_injective arank).
Axiom arank_surjective : (rank_surjective arank).
Axiom arank_bijective : (rank_bijective arank).
Axiom arank_le : (rank_le arank).

Axiom aunrank_injective : (unrank_injective aunrank).
Axiom aunrank_surjective : (unrank_surjective aunrank).
Axiom aunrank_bijective : (unrank_bijective aunrank).
Axiom aunrank_le : (unrank_le aunrank).

Axiom arank_unrank_inv : (rank_unrank_inv arank aunrank).

Lemma arank_argeq : forall a b c d n,
    (arank a b = n) ->
    (arank c d = n) ->
    (a = c) /\ (b = d).
Proof.
  intros a b c d n H1 H2.
  assert (Hinv1 := (arank_unrank_inv a b n)).
  assert (Hinv2 := (arank_unrank_inv c d n)).
  apply Hinv1 in H1.
  apply Hinv2 in H2.
  rewrite H1 in H2.
  injection H2.
  intros. subst. split; reflexivity.
Qed.
