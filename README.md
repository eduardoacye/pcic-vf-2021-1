# Enumeración de programas IMP

El algoritmo de enumeración desarrollado en el proyecto en colaboración con M.A. Vladimir,
L.B. Lluis, V. Zamora y F.H. Quiroz es verificado usando Coq para el lenguaje imperativo IMP.

La sintaxis de IMP es la siguiente:

P -> skip | X := A | (P ; P) | (while B do P) | (if B then P else P)
X -> x[ℕ]
A -> ℕ | X | (A + A) | (A - A) | (A * A)
B -> true | false | (A = A) | (A < A) | not B | (B or B) | (B and B)

La semántica de los programas es bastante intuitiva: skip es un no-op, X := A es la
asignación de memoria, (P ; P) es la ejecución secuencial de programas y el resto de las
construcciones tiene su interpretación usual.

El algoritmo de enumeración de programas se basa en la enumeración de tuplas de naturales,
en el proyecto de trabajo utilizamos un algoritmo para enumerar k-tuplas arbitrarias donde
el orden de las tuplas es tal que la suma de sus componentes aumenta de forma creciente a
lo largo de la enumeración. Para este trabajo de VF se utiliza únicamente enumeraciones de
2-tuplas, considerando que una enumeración no uniforme de k-tuplas se puede obtener a
partir de la tupla `(a, b)` usand a `b` como la posición de una segunda 2-tupla `(c, d)` y
así obtener la 3-tupla `(a, b, c)` o bien, continuar este proceso con `d`.

Las enumeraciones se codifican a partir de dos procesos: `rank` y `unrank`. El `rank` nos
describe cómo obtener la posición de una 2-tupla particular, mientras que `unrank` nos
describe como obtener la 2-tupla en una posición dada.

Estas funciones tienen que ser biyectivas y adicionalmente inversas una de la otra.

Para facilitar las demostraciones de verificación del algoritmo se restringe el espacio de
posibles funciones `rank` y `unrank` a aquel donde las componentes de las 2-tuplas
enumeradas nunca supera el valor de su posición. Sin embargo, esta restricción no es
necesaria para grán parte de la maquinaria del proyecto más amplio.

# Pendientes

- [x] Utilizar `arank` y `aunrank` (a de arbitrario/abstracto/any) en lugar de
      `rank_tuple` y `unrank_tuple` en la enumeración de IMP.
- [x] Factorizar los lemas `nat_multN` en un archivo auxiliar.
- [x] Abstraer `enum_X_rank` y `enum_X_unrank` como propiedades para cualquier tipo `X` y
      relación binaria `X -> nat -> Prop`.
- [ ] Convertir los lemas `nat_multN` en un lema general o bien, en una táctica (se puede?)

# Compilación

Primero se genera el `Makefile` utilizando el comando:

```sh
$ coq_makefile -f _CoqProject -o Makefile
```

Posteriormente, para compilar todos los archivos del proyecto se invoca:

```sh
$ make
```

o bien, para compilar un archivo en particular `File.v`:

```sh
$ make File.vo
```

# Organización del proyecto

- [x] `Tuples_Defs.v`: Firma de funciones rank y unrank utilizadas en el algoritmo de
      enumeración junto con las propiedades que deben de satisfacer.
- [x] `Tuples_Props.v`: Demostración del teorema sobre la existencia de dos funciones que
      satisfacen las propiedades mencionadas en `Tuples_Defs.v` y declaración de los
      parámetros `arank` y `aunrank` para desacoplar los detalles de implementación.

La estrategia utilizada para mostrar la existencia de dos funciones que satisfacen las
propiedades fue definir dos funciones que teóricamente satisfacen las propiedades pero no
son las utilizadas en la práctica debido a la distribución no uniforme de las tuplas en la
enumeración, es decir, el orden de las tuplas `(a, b)` hace que los valores de `b` tiendan
a ser exponencialmente mas grandes que los valores de `a` relativo a la posición `0` de la
enumeración. En el proyecto general, nos referimos a este tipo de enumeración de tuplas
como *sesgado*.

Debido a las características de las funciones de enumeración para 2-tuplas propuesto, se
implementan funciones auxiliares similares a `rank` y `unrank` donde la posición de las
tuplas se codifica con naturales por paridad, mientras que las componentes de las tuplas
se codifican con naturales unarios.

- [x] `NatBNat_Defs.v`: Definición de naturales por paridad y su conversión desde y hasta
      naturales unarios (`nat`).
- [x] `NatBNat_Props.v`: Propiedades auxiliares que relacionan ambos tipos de numerales.
- [x] `MixedRanking_Defs.v`: Definiciones de `mixed_rank` y `mixed_unrank` que utilizan
      codificaciones diferentes para posiciones y componentes de tuplas.
- [x] `MixedRanking_Props.v`: Propiedades auxiliares que facilitan la demostración de las
      propiedades descritas en `Tuples_Defs.v`

Utilizando la conversión entre los dos tipos de numerales utilizados y las propiedades
descritas en los archivos de arriba, se implementan dos funciones que son propiamente
`rank` y `unrank`.

- [x] `Ranking_Defs.v`: Definiciones de funciones que son `rank` y `unrank`.
- [x] `Ranking_Props.v`: Demostración de que las propuestas de enumeración satisfacen las
      propiedades descritas en `Tuples_Defs.v`.

Se establecen una familia de relaciones binarias `R x pos` que asocian a un objeto `x` un
natural `pos`, el objetivo es definir una relación por tipo de expresión en IMP.

- [x] `IMP_Nats_Defs.v`
- [x] `IMP_Nats_Props.v`
- [x] `IMP_Locs_Defs.v`
- [x] `IMP_Locs_Props.v`
- [x] `IMP_Ariths_Defs.v`
- [x] `IMP_Ariths_Props.v`
- [x] `IMP_Bools_Defs.v`
- [x] `IMP_Bools_Props.v`
- [x] `IMP_Progs_Defs.v`
- [x] `IMP_Progs_Props.v`

Finalmente, se implementan dos tácticas simples utilizadas en varias demostraciones para
realizar lo equivalente a un unfold de un paso (sin desdoblar llamadas recursivas) llamado
`unfoldofl` (inicialmente pensado como un palíndromo abreviado de `unfold`, pero que me di
cuenta demasiado tarde que la `l` debe ir en otro lugar).
- [x] `Tactics.v`

