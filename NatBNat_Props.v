From Coq Require Import Arith.Arith.

From Enums Require Import Tactics.
From Enums Require Import NatBNat_Defs.

Lemma nat_to_bnat_dzero : nat_to_bnat (2 * O) = Z.
Proof. simpl. reflexivity. Qed.

Lemma nat_to_bnat_double : forall n, (nat_to_bnat (2 * (S n))) = D (nat_to_bnat n).
Proof.
  intros n. induction n as [| n' IHn].
  - simpl. reflexivity.
  - assert (2 * S (S n') = S (S (2 * (S n')))).
    { simpl. rewrite <- plus_n_O. rewrite <- plus_Snm_nSm.
      rewrite plus_Sn_m. reflexivity. }
    rewrite H. unfoldofl nat_to_bnat.
    rewrite IHn. simpl. reflexivity.
Qed.

Lemma nat_bnat_equiv : forall bn, nat_to_bnat (bnat_to_nat bn) = bn.
Proof.
  intros bn. induction bn as [| bn' IHU | bn' IHD].
  - simpl. reflexivity.
  - unfoldofl bnat_to_nat.
    unfoldofl nat_to_bnat.
    destruct (bnat_to_nat bn').
    + rewrite nat_to_bnat_dzero. simpl in *. apply f_equal. assumption.
    + rewrite nat_to_bnat_double. simpl in *. apply f_equal. assumption.
  - unfoldofl bnat_to_nat.
    unfoldofl nat_to_bnat.
    destruct (bnat_to_nat bn').
    + rewrite nat_to_bnat_dzero. simpl in *. apply f_equal. assumption.
    + rewrite nat_to_bnat_double. simpl in *. apply f_equal. assumption.
Qed.

Lemma bnat_to_nat_succ : forall bn, S (bnat_to_nat bn) = (bnat_to_nat (bnat_succ bn)).
Proof.
  intros bn. induction bn.
  - simpl. reflexivity.
  - simpl in *. reflexivity.
  - unfoldofl bnat_succ.
    unfoldofl bnat_to_nat.
    rewrite <- IHbn. simpl. rewrite <- plus_Snm_nSm.
    rewrite <- plus_n_O. rewrite plus_Sn_m. reflexivity.
Qed.

Lemma bnat_nat_equiv : forall n, bnat_to_nat (nat_to_bnat n) = n.
Proof.
  intros n. induction n as [| n' IHn].
  - simpl. reflexivity.
  - unfoldofl nat_to_bnat.
    destruct (nat_to_bnat n').
    + simpl in *. rewrite <- IHn. reflexivity.
    + simpl in *. apply f_equal. assumption.
    + simpl in *. rewrite <- plus_n_O. rewrite <- plus_n_O in IHn.
      apply f_equal.
      rewrite <- bnat_to_nat_succ.
      rewrite plus_Sn_m.
      rewrite <- plus_Snm_nSm.
      rewrite plus_Sn_m.
      assumption.
Qed.

Lemma bnat_nat_eq_inv : forall bn n, (bnat_to_nat bn) = n -> bn = (nat_to_bnat n).
Proof.
  intros bn n H. rewrite <- H. symmetry. apply nat_bnat_equiv.
Qed.

Lemma nat_bnat_eq_inv : forall n bn, bn = (nat_to_bnat n) -> (bnat_to_nat bn) = n.
Proof.
  intros n bn H. rewrite H. apply bnat_nat_equiv.
Qed.
