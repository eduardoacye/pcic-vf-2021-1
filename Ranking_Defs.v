From Enums Require Import MixedRanking_Defs.
From Enums Require Import NatBNat_Defs.

Definition rank_tuple (a b : nat) : nat := bnat_to_nat (mixed_rank a b).

Definition unrank_tuple (n : nat) : nat * nat := mixed_unrank (nat_to_bnat n).
