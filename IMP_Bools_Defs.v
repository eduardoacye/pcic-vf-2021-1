From Enums Require Import IMP_Ariths_Defs.
From Enums Require Import Tuples_Props.
From Enums Require Import EnumType_Defs.

Inductive Bool : Type :=
| BT
| BF
| BEq (a1 a2 : Arith)
| BLt (a1 a2 : Arith)
| BNot (b : Bool)
| BOr (b1 b2 : Bool)
| BAnd (b1 b2 : Bool).

Inductive enum_bool : (enum_rel Bool) :=
| EB_T : enum_bool BT 0
| EB_F : enum_bool BF 1
| EB_Eq (a1 a2 : Arith)
        (pos1 pos2 : nat)
        (H1 : enum_arith a1 pos1)
        (H2 : enum_arith a2 pos2)
        (pos : nat)
        (H3 : arank pos1 pos2 = pos) :
    enum_bool (BEq a1 a2) (S (S (5 * pos)))
| EB_Lt (a1 a2 : Arith)
        (pos1 pos2 : nat)
        (H1 : enum_arith a1 pos1)
        (H2 : enum_arith a2 pos2)
        (pos : nat)
        (H3 : arank pos1 pos2 = pos) :
    enum_bool (BLt a1 a2) (S (S (S (5 * pos))))
| EB_Not (b : Bool)
         (pos : nat)
         (H : enum_bool b pos) :
    enum_bool (BNot b) (S (S (S (S (5 * pos)))))
| EB_Or (b1 b2 : Bool)
        (pos1 pos2 : nat)
        (H1 : enum_bool b1 pos1)
        (H2 : enum_bool b2 pos2)
        (pos : nat)
        (H3 : arank pos1 pos2 = pos) :
    enum_bool (BOr b1 b2) (S (S (S (S (S (5 * pos))))))
| EB_And (b1 b2 : Bool)
         (pos1 pos2 : nat)
         (H1 : enum_bool b1 pos1)
         (H2 : enum_bool b2 pos2)
         (pos : nat)
         (H3 : arank pos1 pos2 = pos) :
    enum_bool (BAnd b1 b2) (S (S (S (S (S (S (5 * pos))))))).
