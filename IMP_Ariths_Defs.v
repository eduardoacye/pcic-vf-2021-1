From Enums Require Import IMP_Nats_Defs.
From Enums Require Import IMP_Locs_Defs.
From Enums Require Import Tuples_Props.
From Enums Require Import EnumType_Defs.

Inductive Arith : Type :=
| AN (n : nat)
| AX (x : Loc)
| APlus (a1 a2 : Arith)
| AMinus (a1 a2 : Arith)
| AMult (a1 a2 : Arith).

Inductive enum_arith : (enum_rel Arith) :=
| EA_N (n pos : nat)
       (H : enum_nat n pos) :
    enum_arith (AN n) (5 * pos)
| EA_X (x : Loc)
       (pos : nat)
       (H : enum_loc x pos) :
    enum_arith (AX x) (S (5 * pos))
| EA_Plus (a1 a2 : Arith)
          (pos1 pos2 : nat)
          (H1 : enum_arith a1 pos1)
          (H2 : enum_arith a2 pos2)
          (pos : nat)
          (H3 : arank pos1 pos2 = pos) :
    enum_arith (APlus a1 a2) (S (S (5 * pos)))
| EA_Minus (a1 a2 : Arith)
           (pos1 pos2 : nat)
           (H1 : enum_arith a1 pos1)
           (H2 : enum_arith a2 pos2)
           (pos : nat)
           (H3 : arank pos1 pos2 = pos) :
    enum_arith (AMinus a1 a2) (S (S (S (5 * pos))))
| EA_Mult (a1 a2 : Arith)
          (pos1 pos2 : nat)
          (H1 : enum_arith a1 pos1)
          (H2 : enum_arith a2 pos2)
          (pos : nat)
          (H3 : arank pos1 pos2 = pos) :
    enum_arith (AMult a1 a2) (S (S (S (S (5 * pos))))).
